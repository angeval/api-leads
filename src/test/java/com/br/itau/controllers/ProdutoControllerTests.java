package com.br.itau.controllers;

import com.br.itau.models.Produto;
import com.br.itau.services.ProdutoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

@WebMvcTest(ProdutoController.class)
public class ProdutoControllerTests {

    @MockBean
    ProdutoService produtoService;

    @Autowired
    private MockMvc mockMvc;

    ObjectMapper mapper = new ObjectMapper();
    Produto produto;

    @BeforeEach
    public void inicializar(){
        produto = new Produto();
        produto.setNome("Teste de Celular");
        produto.setId(1);
        produto.setDescricao("Celular Xaiomi");
        produto.setPreco(30.00);
    }

    @Test
    public void testarBuscarTodosProdutosSucesso() throws Exception {
        Mockito.when(produtoService.buscarTodosProdutos()).thenReturn(Arrays.asList(produto));

        mockMvc.perform(MockMvcRequestBuilders.get("/produtos")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0]").exists());
    }

    @Test
    public void testarBuscarTodosProdutosVazio() throws Exception {
        Mockito.when(produtoService.buscarTodosProdutos()).thenReturn(new ArrayList<Produto>());

        mockMvc.perform(MockMvcRequestBuilders.get("/produtos")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isEmpty());
    }

    @Test
    public void testarBuscarProdutoSucesso() throws Exception {
        Mockito.when(produtoService.buscarPorId(Mockito.anyInt())).thenReturn(Optional.of(produto));

        int id = 1;

        mockMvc.perform(MockMvcRequestBuilders.get("/produtos/" +id)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id"
                        , CoreMatchers.equalTo(1)));
    }

    @Test
    public void testarBuscarProdutoInexistente() throws Exception {
        Mockito.when(produtoService.buscarPorId(Mockito.anyInt())).thenReturn(Optional.empty());

        int id = 3;

        mockMvc.perform(MockMvcRequestBuilders.get("/produtos/" +id)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    public void testarSalvarProdutoSucesso() throws Exception {
        Mockito.when(produtoService.salvarProduto(Mockito.any(Produto.class))).thenReturn(produto);
        String json = mapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.post("/produtos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id"
                        , CoreMatchers.equalTo(1)));
    }

    @Test
    public void testarSalvarProdutoNomeInvalido() throws Exception {
        Mockito.when(produtoService.salvarProduto(Mockito.any(Produto.class))).thenReturn(produto);
        produto.setDescricao("");
        String json = mapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.post("/produtos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarAtualizarProduto() throws Exception {
        Produto produtoAtualizado = produto;
        produtoAtualizado.setDescricao("Nova Descrição");

        Mockito.when(produtoService.atualizarProduto(Mockito.any(Produto.class))).thenReturn(produtoAtualizado);
        String json = mapper.writeValueAsString(produtoAtualizado);
        int id = 1;

        mockMvc.perform(MockMvcRequestBuilders.put("/produtos/"+id)
                // .param("id", "1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.descricao"
                        , CoreMatchers.equalTo(produtoAtualizado.getDescricao())));
    }
    @Test
    public void testarRemoverProdutoInexistente() throws Exception {
        Mockito.when(produtoService.buscarPorId(Mockito.anyInt())).thenReturn(Optional.empty());

        int id = 3;

        mockMvc.perform(MockMvcRequestBuilders.delete("/produtos/" +id)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    public void testarRemoverProdutoSucesso() throws Exception {
        Mockito.when(produtoService.buscarPorId(Mockito.anyInt())).thenReturn(Optional.of(produto));

        int id = 1;
        String json = mapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.delete("/produtos/" +id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

}
