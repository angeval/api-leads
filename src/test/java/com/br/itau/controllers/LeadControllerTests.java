package com.br.itau.controllers;

import com.br.itau.enums.TipoDeLead;
import com.br.itau.models.Lead;
import com.br.itau.models.Produto;
import com.br.itau.services.LeadService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.Arrays;

@WebMvcTest(LeadController.class)
public class LeadControllerTests {

    @MockBean
    LeadService leadService;

    @Autowired
    private MockMvc mockMvc;

    ObjectMapper mapper = new ObjectMapper();
    Lead lead;
    Produto produto;

    @BeforeEach
    public void inicializar(){
        produto = new Produto();
        produto.setNome("Celular");
        produto.setId(1);
        produto.setDescricao("Celular Xaiomi");
        produto.setPreco(30.00);

        lead = new Lead();
        lead.setNome("Angela");
        lead.setEmail("angela@itau.com.br");
        lead.setTipoDeLead(TipoDeLead.FRIO);
        lead.setProdutos(Arrays.asList(produto));
    }

    @Test
    public void testarBuscarTodosLeads() throws Exception {
        Mockito.when(leadService.buscarTodosLeads()).thenReturn(Arrays.asList(lead));

        mockMvc.perform(MockMvcRequestBuilders.get("/leads")
                // .param("id", "1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0]").exists());
    }

    @Test
    public void testarBuscarTodosLeadsVazio() throws Exception {
        Mockito.when(leadService.buscarTodosLeads()).thenReturn(new ArrayList<Lead>());

        mockMvc.perform(MockMvcRequestBuilders.get("/leads")
                // .param("id", "1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isEmpty());
    }

    @Test
    public void testarBuscarLeadSucesso() throws Exception {
        Mockito.when(leadService.buscarPorId(Mockito.anyInt())).thenReturn(lead);

        int id = 1;

        mockMvc.perform(MockMvcRequestBuilders.get("/leads/" +id)
                // .param("id", "1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.produtos[0].id"
                        , CoreMatchers.equalTo(1)));
    }

    @Test
    public void testarBuscarLeadInexistente() throws Exception {
        Mockito.when(leadService.buscarPorId(Mockito.anyInt())).thenReturn(null);

        int id = 3;

        mockMvc.perform(MockMvcRequestBuilders.get("/leads/" +id)
                // .param("id", "1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    public void testarInserirLeadSucesso() throws Exception {
        Iterable<Produto> produtoIterable = Arrays.asList(produto);

        Mockito.when(leadService.inserirLead(Mockito.any(Lead.class))).thenReturn(lead);
        Mockito.when(leadService.buscarTodosProdutos(Mockito.anyList())).thenReturn(produtoIterable);

        String json = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.produtos[0].id"
                        , CoreMatchers.equalTo(1)));

    }

    @Test
    public void testarInserirLeadErroNome() throws Exception {
        Iterable<Produto> produtoIterable = Arrays.asList(produto);
        lead.setNome("");
        Mockito.when(leadService.inserirLead(Mockito.any(Lead.class))).thenReturn(lead);
        Mockito.when(leadService.buscarTodosProdutos(Mockito.anyList())).thenReturn(produtoIterable);

        String json = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarInserirLeadErroEmail() throws Exception {
        Iterable<Produto> produtoIterable = Arrays.asList(produto);
        lead.setEmail("xx");
        Mockito.when(leadService.inserirLead(Mockito.any(Lead.class))).thenReturn(lead);
        Mockito.when(leadService.buscarTodosProdutos(Mockito.anyList())).thenReturn(produtoIterable);

        String json = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarInserirLeadErroEmailVazio() throws Exception {
        Iterable<Produto> produtoIterable = Arrays.asList(produto);
        lead.setEmail("");
        Mockito.when(leadService.inserirLead(Mockito.any(Lead.class))).thenReturn(lead);
        Mockito.when(leadService.buscarTodosProdutos(Mockito.anyList())).thenReturn(produtoIterable);

        String json = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarInserirLeadErroNomeVazio() throws Exception {
        Iterable<Produto> produtoIterable = Arrays.asList(produto);
        Mockito.when(leadService.inserirLead(Mockito.any(Lead.class))).thenReturn(lead);
        Mockito.when(leadService.buscarTodosProdutos(Mockito.anyList())).thenReturn(produtoIterable);
        lead.setNome(null);

        String json = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarAtualizarLead() throws Exception {
        Lead leadAtualizado = lead;
        leadAtualizado.setEmail("novo@email.com.br");

        Mockito.when(leadService.atualizarLead(Mockito.any(Lead.class))).thenReturn(leadAtualizado);
        String json = mapper.writeValueAsString(leadAtualizado);
        int id = 1;

        mockMvc.perform(MockMvcRequestBuilders.put("/leads/"+id)
                // .param("id", "1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.email"
                        , CoreMatchers.equalTo(leadAtualizado.getEmail())));
    }

    @Test
    public void testarRemoverLeadInexistente() throws Exception {
        Mockito.when(leadService.buscarPorId(Mockito.anyInt())).thenReturn(null);

        int id = 3;

        mockMvc.perform(MockMvcRequestBuilders.delete("/leads/" +id)
                // .param("id", "1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    public void testarRemoverLeadSucesso() throws Exception {
        Mockito.when(leadService.buscarPorId(Mockito.anyInt())).thenReturn(lead);

        int id = 1;
        String json = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.delete("/leads/" +id)
                // .param("id", "1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

}
