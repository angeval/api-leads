package com.br.itau.services;

import com.br.itau.models.Lead;
import com.br.itau.models.Produto;
import com.br.itau.repositories.ProdutoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.Optional;

@SpringBootTest
public class ProdutoServiceTests {
    @MockBean
    ProdutoRepository produtoRepository;

    @Autowired
    ProdutoService produtoService;

    Produto produto;

    @BeforeEach
    public void inicializarTestes() {
        produto = new Produto();
        produto.setId(1);
        produto.setNome("Teste Lead");
        produto.setPreco(30.00);
        produto.setDescricao("Top teste");
    }

    @Test
    public void testarBuscarPorIdSucesso(){
        Mockito.when(produtoRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(produto));
        int id = 1;
        Optional produtoOptional = produtoService.buscarPorId(id);
        Assertions.assertEquals(produto, produtoOptional.get());
    }

    @Test
    public void testarBuscarPorIdInexistente(){
        Mockito.when(produtoRepository.findById(Mockito.anyInt())).thenReturn(Optional.empty());
        int id = 1;
        Optional produtoOptional = produtoService.buscarPorId(id);
        Assertions.assertFalse(produtoOptional.isPresent());
    }

    @Test
    public void testarSalvarProdutoSucesso(){
        Mockito.when(produtoRepository.save(Mockito.any(Produto.class))).thenReturn(produto);
        Produto produtoObjeto = produtoService.salvarProduto(produto);
        Assertions.assertEquals(produto, produtoObjeto);
    }

    @Test
    public void testarBuscarTodosProdutosSucesso(){
        Iterable<Produto> produtoIterable = Arrays.asList(produto);
        Mockito.when(produtoRepository.findAll()).thenReturn(produtoIterable);
        Iterable<Produto> iterableResultado = produtoService.buscarTodosProdutos();
        Assertions.assertEquals(produtoIterable, iterableResultado);
    }

    @Test
    public void testarBuscarTodosProdutosVazio(){
        Iterable<Produto> produtoIterable = Arrays.asList(new Produto());
        Mockito.when(produtoRepository.findAll()).thenReturn(produtoIterable);
        Iterable<Produto> iterableResultado = produtoService.buscarTodosProdutos();
        Assertions.assertEquals(produtoIterable, iterableResultado);
    }
    @Test
    public void testarAtualizarProdutoSucesso(){
        Mockito.when(produtoRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(produto));
        Mockito.when(produtoRepository.save(Mockito.any(Produto.class))).thenReturn(produto);
        Produto produtoObjeto = produtoService.atualizarProduto(produto);
        Assertions.assertEquals(produto, produtoObjeto);
    }

    @Test
    public void testarAtualizarProdutoInexistente(){
        Mockito.when(produtoRepository.findById(Mockito.anyInt())).thenReturn(Optional.empty());
        Mockito.when(produtoRepository.save(Mockito.any(Produto.class))).thenReturn(null);
        Produto produtoObjeto = produtoService.atualizarProduto(produto);
        Assertions.assertNull(produtoObjeto);
    }

    @Test
    public void testarAtualizarProdutoNomeInvalido(){
        Produto produtoAtualizado = produto;
        produtoAtualizado.setNome(null);
        Mockito.when(produtoRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(produto));
        Mockito.when(produtoRepository.save(Mockito.any(Produto.class))).thenReturn(produtoAtualizado);
        Produto produtoObjeto = produtoService.atualizarProduto(produtoAtualizado);
        Assertions.assertEquals(produtoObjeto, produtoAtualizado);
    }

    @Test
    public void testarAtualizarProdutoDescricaoInvalido(){
        Produto produtoAtualizado = produto;
        produtoAtualizado.setDescricao(null);
        Mockito.when(produtoRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(produto));
        Mockito.when(produtoRepository.save(Mockito.any(Produto.class))).thenReturn(produtoAtualizado);
        Produto produtoObjeto = produtoService.atualizarProduto(produtoAtualizado);
        Assertions.assertEquals(produtoObjeto, produtoAtualizado);
    }

    @Test
    public void testarAtualizarProdutoPrecoInvalido(){
        Produto produtoAtualizado = produto;
        produtoAtualizado.setPreco(null);
        Mockito.when(produtoRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(produto));
        Mockito.when(produtoRepository.save(Mockito.any(Produto.class))).thenReturn(produtoAtualizado);
        Produto produtoObjeto = produtoService.atualizarProduto(produtoAtualizado);
        Assertions.assertEquals(produtoObjeto, produtoAtualizado);
    }

    @Test
    public void testarRemoverProdutoSucesso(){
        produtoService.deletarProduto(produto);
        Mockito.verify(produtoRepository, Mockito.times(1))
                .delete(Mockito.any(Produto.class));
    }
}
