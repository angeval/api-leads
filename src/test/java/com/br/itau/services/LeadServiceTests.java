package com.br.itau.services;

import com.br.itau.enums.TipoDeLead;
import com.br.itau.models.Lead;
import com.br.itau.models.Produto;
import com.br.itau.repositories.LeadRepository;
import com.br.itau.repositories.ProdutoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class LeadServiceTests {
    @MockBean
    LeadRepository leadRepository;

    @MockBean
    ProdutoRepository produtoRepository;

    @Autowired
    LeadService leadService;

    Lead lead;
    Produto produto;

    @BeforeEach
    public void inicializarTestes(){
        produto = new Produto();
        produto.setId(1);
        produto.setNome("Teste Lead");
        produto.setPreco(30.00);
        produto.setDescricao("Top teste");

        lead = new Lead();
        lead.setId(1);
        lead.setNome("Teste");
        lead.setEmail("angeval@itau.com.br");
        lead.setTipoDeLead(TipoDeLead.FRIO);
        lead.setProdutos(Arrays.asList(new Produto()));
        lead.setProdutos(Arrays.asList(produto));
    }

    @Test
    public void testarBuscarPorIdSucesso(){
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(lead));
        int id = 1;

        lead.setId(id);
        Lead leadObjeto = leadService.buscarPorId(id);

        Assertions.assertEquals(lead, leadObjeto);
        Assertions.assertEquals(lead.getNome(), leadObjeto.getNome());
        Assertions.assertEquals(lead.getEmail(), leadObjeto.getEmail());
        Assertions.assertEquals(lead.getTipoDeLead(), leadObjeto.getTipoDeLead());
    }

    @Test
    public void testarBuscarPorIdVazio(){
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(Optional.empty());
        int id = 6;

        lead.setId(id);
        Lead leadObjeto = leadService.buscarPorId(id);

        Assertions.assertNull(leadObjeto);
    }

    @Test
    public void testarBuscarPorIdInexistente(){
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(Optional.empty());
        int id = 3;

        lead.setId(id);
        Lead leadObjeto = leadService.buscarPorId(id);
        Assertions.assertNull(leadObjeto);
    }

    @Test
    public void testarBuscarTodosProdutosSucesso(){
        Iterable<Produto> produtoIterable = Arrays.asList(produto);
        Mockito.when(produtoRepository.findAllById(Mockito.anyIterable())).thenReturn(produtoIterable);
        List<Integer> produtosId = new ArrayList<>();
        produtosId.add(1);
        Iterable<Produto> iterableResultado =leadService.buscarTodosProdutos(produtosId);
        Assertions.assertEquals(produtoIterable, iterableResultado);
    }

    @Test
    public void testarBuscarTodosProdutosVazio(){
        Iterable<Produto> produtoIterable = Arrays.asList(new Produto());
        Mockito.when(produtoRepository.findAllById(Mockito.anyIterable())).thenReturn(produtoIterable);
        List<Integer> produtosId = new ArrayList<>();
        produtosId.add(1);
        Iterable<Produto> iterableResultado =leadService.buscarTodosProdutos(produtosId);
        Assertions.assertEquals(produtoIterable, iterableResultado);
    }

    @Test
    public void testarInserirLeadSucesso(){
        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(lead);
        Lead leadObjeto = leadService.inserirLead(lead);
        Assertions.assertEquals(lead, leadObjeto);
        Assertions.assertEquals(lead.getEmail(), leadObjeto.getEmail());
    }

    @Test
    public void testarBuscarTodosLeadsSucesso(){
        Iterable<Lead> leadIterable = Arrays.asList(lead);
        Mockito.when(leadRepository.findAll()).thenReturn(leadIterable);
        Iterable<Lead> iterableResultado =leadService.buscarTodosLeads();
        Assertions.assertEquals(leadIterable, iterableResultado);
    }

    @Test
    public void testarAtualizarLeadSucesso(){
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(lead));
        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(lead);
        Lead leadObjeto = leadService.atualizarLead(lead);
        Assertions.assertEquals(lead, leadObjeto);
    }

    @Test
    public void testarAtualizarLeadInexistente(){
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(Optional.empty());
        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(null);
        Lead leadObjeto = leadService.atualizarLead(lead);
        Assertions.assertNull(leadObjeto);
    }

    @Test
    public void testarAtualizarLeadNomeInvalido(){
        Lead leadAtualizado = lead;
        leadAtualizado.setNome(null);
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(lead));
        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(leadAtualizado);
        Lead leadObjeto = leadService.atualizarLead(leadAtualizado);
        Assertions.assertEquals(leadObjeto, leadAtualizado);
    }

    @Test
    public void testarAtualizarLeadEmailInvalido(){
        Lead leadAtualizado = lead;
        leadAtualizado.setEmail(null);
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(lead));
        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(leadAtualizado);
        Lead leadObjeto = leadService.atualizarLead(leadAtualizado);
        Assertions.assertEquals(leadObjeto, leadAtualizado);
    }

    @Test
    public void testarAtualizarLeadTipoDeLeadInvalido(){
        Lead leadAtualizado = lead;
        leadAtualizado.setTipoDeLead(null);
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(lead));
        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(leadAtualizado);
        Lead leadObjeto = leadService.atualizarLead(leadAtualizado);
        Assertions.assertEquals(leadObjeto, leadAtualizado);
    }

    @Test
    public void testarRemoverLeadSucesso(){
        leadService.removerLead(lead);
        Mockito.verify(leadRepository, Mockito.times(1)).delete(Mockito.any(Lead.class));
    }

}
