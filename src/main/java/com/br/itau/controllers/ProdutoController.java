package com.br.itau.controllers;
import com.br.itau.models.Produto;
import com.br.itau.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    @Autowired
    private ProdutoService produtoService;

    @GetMapping
    public Iterable<Produto> buscarTodosProdutos(){
        return produtoService.buscarTodosProdutos();
    }

    @GetMapping("/{id}")
    public Produto buscarProduto(@PathVariable Integer id){
        Optional<Produto> produtoOptional = produtoService.buscarPorId(id);

        if(produtoOptional.isPresent()){
            return produtoOptional.get();
        }else{
            throw new ResponseStatusException(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping
    public ResponseEntity<Produto> salvarProduto(@RequestBody @Valid Produto produto){
        Produto leadObjeto = produtoService.salvarProduto(produto);
        return ResponseEntity.status(201).body(leadObjeto);
    }

    @PutMapping("/{id}")
    public Produto atualizarProduto(@PathVariable Integer id, @RequestBody Produto produto){
        produto.setId(id);
        Produto leadObjeto = produtoService.atualizarProduto(produto);
        return leadObjeto;
    }

    @DeleteMapping("/{id}")
    public Produto deletarProduto(@PathVariable Integer id){
        Optional<Produto> produtoOptional = produtoService.buscarPorId(id);
        if(produtoOptional.isPresent()){
            produtoService.deletarProduto(produtoOptional.get());
            return produtoOptional.get();
        }
        throw new ResponseStatusException(HttpStatus.NO_CONTENT);
    }

}
