package com.br.itau.controllers;

import com.br.itau.enums.TipoDeLead;
import com.br.itau.models.Lead;
import com.br.itau.models.Produto;
import com.br.itau.services.LeadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RequestMapping("/leads")
@RestController
public class LeadController {

    @Autowired
    //instanciar essa classe para todas as classes
    private LeadService leadService;

    @GetMapping
    //Buscar todos da lista
    public Iterable<Lead> buscarTodosLeads(){
        return leadService.buscarTodosLeads();
    }

    @GetMapping("/{id}")
    public Lead buscarLead(@PathVariable int id){
    //PathVariable vai indicar que ele vai receber essa variável da URL
        Lead lead = leadService.buscarPorId(id);
        if (lead==null){
            throw new ResponseStatusException(HttpStatus.NO_CONTENT);
        }
        return lead;
    }

    @PostMapping
    //Insere na base
    public ResponseEntity<Lead> inserirLead(@RequestBody @Valid Lead lead){
        //Foi para a entidade model
        if(lead.getNome()==null)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Informe um nome válido");
        if(lead.getEmail()==null || lead.getEmail().length()<3)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Informe um e-mail válido");
        List<Integer> produtosId = new ArrayList<>();
        for(Produto produto: lead.getProdutos()){
            produtosId.add(produto.getId());
        }
        Iterable<Produto> produtosIterable = leadService.buscarTodosProdutos(produtosId);
        lead.setProdutos((List) produtosIterable);

        Lead leadObjeto = leadService.inserirLead(lead);
        return ResponseEntity.status(201).body(leadObjeto);
    }

    @PutMapping("/{id}")
    //Atualiza registro na base
    public ResponseEntity<Lead> atualizarLead(@PathVariable Integer id, @RequestBody Lead lead){
        //if(lead.getNome().isEmpty())
        //    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Informe um nome válido");
        //if(lead.getEmail().isEmpty())
        //    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Informe um e-mail válido");
        //if(!TipoDeLead.isValidTipoDeLead(lead.getTipoDeLead().toString())){
        //    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Informe um tipo de lead válido ");
        //}
        lead.setId(id);
        Lead leadObjeto = leadService.atualizarLead(lead);
        return ResponseEntity.status(200).body(leadObjeto);
    }

    @DeleteMapping("/{id}")
    //Deleta registro da base
    public ResponseEntity<Lead> removerLead(@PathVariable Integer id){
        Lead lead = leadService.buscarPorId(id);
        if (lead==null){
            throw new ResponseStatusException(HttpStatus.NO_CONTENT);
        }
        else{
            leadService.removerLead(lead);
        }
        return ResponseEntity.status(200).body(lead);
    }
}
