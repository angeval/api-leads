package com.br.itau.services;

import com.br.itau.models.Produto;
import com.br.itau.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
    private ProdutoRepository produtoRepository;

    public Optional<Produto> buscarPorId(int id){
        Optional<Produto> produtoOptional = produtoRepository.findById(id);
        return produtoOptional;
    }

    public Produto salvarProduto(Produto produto){
        Produto produtoObjeto = produtoRepository.save(produto);
        return produtoObjeto;
    }

    public Iterable<Produto> buscarTodosProdutos(){
        Iterable<Produto> produto = produtoRepository.findAll();
        return produto;
    }

    public Produto atualizarProduto(Produto produto){
        Optional<Produto> produtoOptional = buscarPorId(produto.getId());
        if (produtoOptional.isPresent()){
            Produto produtoData = produtoOptional.get();

            if(produto.getNome() == null){
                produto.setNome(produtoData.getNome());
            }
            if(produto.getDescricao() == null){
                produto.setDescricao(produtoData.getDescricao());
            }
            if(produto.getPreco() == null){
                produto.setPreco(produtoData.getPreco());
            }
        }
        Produto produtoObjeto = produtoRepository.save(produto);
        return produtoObjeto;
    }

    public void deletarProduto(Produto produto){
        produtoRepository.delete(produto);
    }
}
