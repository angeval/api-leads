package com.br.itau.services;

import com.br.itau.models.Lead;
import com.br.itau.models.Produto;
import com.br.itau.repositories.LeadRepository;
import com.br.itau.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LeadService {

    @Autowired
    private LeadRepository leadRepository;

    @Autowired
    private ProdutoRepository produtoRepository;

    public Lead buscarPorId(int id) {
        Optional<Lead> leadOptional = leadRepository.findById(id);
        Lead lead = null;
        if (leadOptional.isPresent()) {
            lead = leadOptional.get();
        }
        return lead;
    }

    public Iterable<Produto> buscarTodosProdutos(List<Integer> produtosId){
        Iterable<Produto> produtoIterable = produtoRepository.findAllById(produtosId);
        return produtoIterable;
    }

    public Lead inserirLead(Lead lead){
        Lead leadObjeto = leadRepository.save(lead);
        return leadObjeto;
    }

    public Iterable<Lead> buscarTodosLeads(){
        Iterable<Lead> leads = leadRepository.findAll();
        return leads;
    }

    public Lead atualizarLead(Lead lead){
        Optional<Lead> leadOptional = leadRepository.findById(lead.getId());
        if(leadOptional.isPresent()) {
            Lead leadData = leadOptional.get();
            if (lead.getNome()== null) {
                lead.setNome(leadData.getNome());
            }
            if (lead.getEmail() == null) {
                lead.setEmail(leadData.getEmail());
            }
            if (lead.getTipoDeLead() == null) {
                lead.setTipoDeLead(leadData.getTipoDeLead());
            }
        }
        Lead leadObjeto = leadRepository.save(lead);
        return leadObjeto;
    }

    public void removerLead(Lead lead){
        leadRepository.delete(lead);
    }

}
