package com.br.itau.repositories;

import com.br.itau.models.Lead;
import org.springframework.data.repository.CrudRepository;

public interface LeadRepository extends CrudRepository<Lead, Integer> {

}
