package com.br.itau.enums;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public enum TipoDeLead {
    QUENTE, ORGANICO, FRIO;

    private final static Set<String> values = new HashSet<String>(TipoDeLead.values().length);

    static{
        for(TipoDeLead f: TipoDeLead.values())
            values.add(f.name());
    }

    //public static boolean contains( String value ){
    //    return values.contains(value);
    //}

    //public static boolean isValidTipoDeLead(String tipoDeLead) {
    //    return Arrays.stream(TipoDeLead.values())
    //            .map(TipoDeLead::name)
    //            .collect(Collectors.toSet())
    //            .contains(tipoDeLead);
    // }
}
